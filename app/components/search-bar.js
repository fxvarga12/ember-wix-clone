import Ember from 'ember';
// import DetectOusideClick from '../mixins/detect-outside-click';
// DetectOusideClick,
export default Ember.Component.extend({
    classNames:['m-search'],
    isActive:false,
    showSettings:false,
    currentValue:'',

    filteredData: Ember.computed('data.@each','currentValue', function(){
        var self = this;
        var filteredList = this.get("data").filter(function(item) {
            var needle = self.get('currentValue').toLowerCase();
            var haystack = item.title.toLowerCase();
            return haystack.indexOf(needle) !== -1;
        });
        return this.get('currentValue') !== '' ? filteredList : this.get("data");
    }),
    onOutsideClick:function(){
        this.set( 'isActive', false );
    },
    didUpdate:function(){
        this.$('#search').focus();
    },
    actions: {
        toggleActive(){
            if( !this.isActive ){
                this.toggleProperty('isActive');
            }
        },
        onSetSelectedItem(string){
            this.onSetSelectedItem(string);
        },
        setActiveState(){
            this.onOutsideClick();
        },
        onChanged(value){
            this.set('currentValue', value);
        },
        setReorderedItems(items){
            this.set('data', items);
        },
        setSettingsState(active){
            this.set('showSettings',active);
        }
    }
});
