import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ["m-pagesFinder"],
	settingsSelected:false,
	sortedData: Ember.computed('data@each', function() {
		return this.get('data');
	}),
	actions: {
		closeModal() {
			this.onCloseFinder();
		},
		unsetHovered() {
			this.get('data').setEach('isHovered', false);
		},
		setHovered(item) {
			this.get('data').setEach('isHovered', false);
			this.get('data').findBy('uri', item.uri).set('isHovered', true);
		},
		setSelected(item) {
			this.get('data').setEach('isActive', false);
			this.get('data').findBy('uri', item.uri).set('isActive', true);
			this.onSelect(item.uri);
		},
		reorderItems(group) {
			this.onReorder(group);
		},
		onSettingsClick(item){
			this.onSettingsClick(item);
		}
	}
});