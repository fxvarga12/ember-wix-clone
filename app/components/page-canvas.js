import Ember from 'ember';

export default Ember.Component.extend({
	classNames:['m-pageCanvas'],
	mainContentItems:Ember.computed('data','data.content','data.content.@each',function(){
		var self = this;
		var filteredList = this.get("data.content").filter(function(item) {
		    var needle = 'main'
		    var haystack = item.marker.toLowerCase();
		    return haystack.indexOf(needle) !== -1;
		});
		return filteredList;
	}),
	actions:{
		addToHeader(){
			console.log('add to header')
		},
		addToMain(){
			console.log('add to main')
		},
		addToFooter(){
			console.log('add to footer')
		},
	}
});
