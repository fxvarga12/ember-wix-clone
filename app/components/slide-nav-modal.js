import Ember from 'ember';
export default Ember.Component.extend({
	tagName:"div",
	isFirst:Ember.computed('position', function(){
		return this.position === 0;
	}),
	actions: {
		closeModal(){
			this.sendAction();
		}
	}
});
