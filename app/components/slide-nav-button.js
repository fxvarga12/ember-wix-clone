import Ember from 'ember';

export default Ember.Component.extend({
	classNames:['m-sidenav__item'],
	isFirst:Ember.computed('position', function(){
		return this.position === 0;
	}),
	actions: {
		toggleActive() {
			if( !this.isActive ){
				this.toggleProperty('isActive');
			}
			if( this.isActive ){
				this.sendAction( "action", this.identifier );
			}
		}
	}
});
