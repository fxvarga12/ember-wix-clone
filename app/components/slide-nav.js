import Ember from 'ember';
import DetectOusideClick from '../mixins/detect-outside-click';
export default Ember.Component.extend( DetectOusideClick,{
	tagName:"div",
	classNames:["m-sidenav__wrapper"],
	noHover: Ember.computed('model.@each.isActive', function(){
		var hasActive = this.get("model").filterBy('isActive',true);
		return hasActive;
	}),
	onOutsideClick:function(){
		this.get('model').setEach('isActive',false);
	},
	actions: {
		setActiveButton(identifier) {
			this.get('model').setEach('isActive',false);
			this.get('model').findBy('identifier', identifier).set('isActive', true);
		},
		clearActiveButton() {
			this.onOutsideClick();
		}
	}
});
