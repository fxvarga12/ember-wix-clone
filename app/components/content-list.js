import Ember from 'ember';

export default Ember.Component.extend({
	sortableObjectList: '',
	init(){
		this._super(...arguments);
		const content = [
		Ember.Object.create({
			isActive: false,
			name:"Spotlight",
			uri:'spotlight'
		}),
		Ember.Object.create({
			isActive: false,
			name:"Linked List",
			uri:"linked-list"
		}),
		Ember.Object.create({
			isActive: false,
			name:"FAQ",
			uri:'faq'
		})
		];
		this.set('sortableObjectList', content);
	}
});
