import Ember from 'ember';

export default Ember.Component.extend({
	classNames:['m-pageSettings'],
	actions:{
		closeModal(){
			this.onCloseSettings();
		}
	}
});
