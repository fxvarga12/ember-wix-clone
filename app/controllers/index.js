import Ember from 'ember';

export default Ember.Controller.extend({
	selectedItemString: 'homepage',
	selectedItem: Ember.computed('data@each','selectedItemString', function(){
	    var condition = this.get('selectedItemString');
	    return this.get('data').findBy('uri', condition);
	}),
	actions:{
		onSetSelectedItem(item){
			this.set('selectedItemString', item);
		}
	}
});
