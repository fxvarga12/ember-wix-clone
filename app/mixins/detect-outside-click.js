import Ember from 'ember';

var DetectOutsiteClick = Ember.Mixin.create({
	onOutsideClick: Ember.K,

	handleOutsideClick: function(event) {

		let $element = this.$();
		let $target = $(event.target);

		if (!$target.closest($element).length) {
			this.onOutsideClick();
		}
	},

	setupOutsideClickListener: Ember.on('didInsertElement', function() {

		let clickHandler = this.get('handleOutsideClick').bind(this);

		return Ember.$(document).on('click', clickHandler);
	}),

	removeOutsideClickListener: Ember.on('willDestroyElement', function() {

		let clickHandler = this.get('handleOutsideClick').bind(this);

		return Ember.$(document).off('click', Ember.run.cancel(this, clickHandler));
	})
});

export default DetectOutsiteClick;