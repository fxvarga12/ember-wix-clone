import Ember from 'ember';

export default Ember.Route.extend({
    setupController(controller, model) {
        const pages = [
            Ember.Object.create({
                isActive: false,
                uri: "about-us",
                title: "About Us",
                position: 1,
                content:[
                    Ember.Object.create({
                        type: "spotlight",
                        position: 1,
                        marker:'main'
                    })
                ]
            }),
            Ember.Object.create({
                isActive: false,
                uri: "homepage",
                title: "Homepage",
                position: 0,
                content:[
                    Ember.Object.create({
                        type: "spotlight",
                        position: 1,
                        marker:'main'
                    }),
                    Ember.Object.create({
                        type: "linked_list",
                        position: 1,
                        marker:'main'
                    }),
                ]
            }),
            Ember.Object.create({
                isActive: false,
                uri: "contact-us",
                title: "Contact Us",
                position: 2,
                content:[]
            })
        ];
        controller.set('model', model);
        controller.set('data', pages);
    },
    model() {
        return [

            Ember.Object.create({
                isActive: false,
                identifier: "content",
                icon: "pencil",
                label: "Edit Contents"
            }),
            Ember.Object.create({
                isActive: false,
                identifier: "images",
                icon: "camera",
                label: "Show Images"
            }),
            Ember.Object.create({
                isActive: false,
                identifier: "layout",
                icon: "archive",
                label: "Edit Layout"
            }),
            Ember.Object.create({
                isActive: false,
                identifier: "store",
                icon: "shopping-cart",
                label: "Marketplace"
            }),
            Ember.Object.create({
                isActive: false,
                identifier: "pages",
                icon: "envelope",
                label: "View Messages"
            })
        ];
    }
});