import Ember from 'ember';
import DetectOutsideClickMixin from 'wix/mixins/detect-outside-click';
import { module, test } from 'qunit';

module('Unit | Mixin | detect outside click');

// Replace this with your real tests.
test('it works', function(assert) {
  let DetectOutsideClickObject = Ember.Object.extend(DetectOutsideClickMixin);
  let subject = DetectOutsideClickObject.create();
  assert.ok(subject);
});
